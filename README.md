# Explorer (Game, Unity Project)

## Instructions

Exploration based game with spin-off on standard 52 card Poker deck. Project contains full Unity project with original Blender files for used models.

For instructions how to play the game, read "instructions.pdf".


## Copyrights

This work is licensed under a Creative Commons Attribution 4.0 International License.

[![Alt text](./by.png?raw=true "CC_copyright")](https://creativecommons.org/licenses/by/4.0/)