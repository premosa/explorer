﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpecialToken : Token
{
    public override int polarity
    {
        get
        {
            return 3;
        }
    }

    public readonly static int SPECIAL_TOKEN_COUNT = 1;
}
