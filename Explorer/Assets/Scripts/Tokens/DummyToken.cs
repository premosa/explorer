﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyToken : Token
{

    public override int polarity
    {
        get
        {
            return 0;
        }
    }

    public override void activateToken(Board board)
    {
        Debug.Log("Dummy token clicked... No effect."); // TODO: comment this
    }
}
