﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RerollTokensToken : SpecialToken
{
    public override void activateToken(Board board)
    {
        GameLogic gamelogic = Camera.main.GetComponent<GameLogic>();
        List<ActiveNodes> active_nodes = gamelogic.returnListOfActiveNodes();
        int i = 0;
        for (; i < active_nodes.Count; i++) {
            gamelogic.returnNodeOfPosition(active_nodes[i].layer, active_nodes[i].node).rerandomToken();
        }
    }
}
