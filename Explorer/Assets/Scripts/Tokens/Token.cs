﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Token {

    public virtual int polarity { get; set; }

    public GameObject physical_object;

    public abstract void activateToken(Board board);
}
