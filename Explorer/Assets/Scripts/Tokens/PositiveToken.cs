﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PositiveToken : Token
{
    public override int polarity
    {
        get
        {
            return 1;
        }
    }

    public readonly static int POSITIVE_TOKEN_COUNT = 3;
}
