﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropCardTypeToken : NegativeToken
{
    public dropCardTypeToken(char new_card_type) {
        card_type = new_card_type;
    }

    public char card_type { get; set; }

    public override void activateToken(Board board)
    {
        // Drop all hearts
        int i = 0;
        while (true)
        {
            if (i >= board.player.player_hand.current_hand_size) { break; }
            if (board.player.player_hand.current_hands[i].getSuite() == card_type)
            {
                //if current hand of iteration is == card_type, then drop this card
                board.player.player_hand.dropCard(i);
            }
            else {
                //else, move to next card
                i++;
            }
        }
    }
}
