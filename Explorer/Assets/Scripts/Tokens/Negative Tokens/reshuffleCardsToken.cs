﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reshuffleCardsToken : NegativeToken
{
    public override void activateToken(Board board)
    {
        //temporarily save card count, then clear current hand
        int card_count = board.player.player_hand.current_hand_size;
        while (board.player.player_hand.current_hands.Count > 0) {
            board.player.player_hand.dropCard(0);
        }
        board.player.player_hand.current_hand_size = 0;

        //now redraw same amount of cards as there were initially
        board.player.player_hand.drawCards(card_count);
    }
}
