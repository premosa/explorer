﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropCardToken : NegativeToken
{
    public override void activateToken(Board board)
    {
        //randomize card index to drop
        int card_index = Random.Range(0,board.player.player_hand.current_hand_size);
        board.player.player_hand.dropCard(card_index);
    }
}
