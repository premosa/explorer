﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NegativeToken : Token
{
    public override int polarity
    {
        get
        {
            return 2;
        }
    }

    public readonly static int NEGATIVE_TOKEN_COUNT = 3;
}
