﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawUntilXCardsToken : PositiveToken
{
    public override void activateToken(Board board)
    {
        int current_hand_size = board.player.player_hand.current_hand_size;
        if (current_hand_size < 4) {
            int draw_count = 4 - current_hand_size;
            board.player.player_hand.drawCards(draw_count);
        }
    }
}
