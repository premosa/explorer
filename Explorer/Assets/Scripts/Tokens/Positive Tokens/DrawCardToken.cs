﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawCardToken : PositiveToken
{
    public override void activateToken(Board board)
    {
        board.player.player_hand.drawCards(1);
    }
}
