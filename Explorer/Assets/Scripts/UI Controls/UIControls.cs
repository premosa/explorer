﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class UIControls : MonoBehaviour
{

    private bool isUIControlSet = false;

    // EDITOR: Set this in Unity Editor
    public List<char> texture_ranks { get; set; }
    public Texture[] texture_hearts = new Texture[13];
    public Texture[] texture_diamonds = new Texture[13];
    public Texture[] texture_clubs = new Texture[13];
    public Texture[] texture_spades = new Texture[13];

    // EDITOR: Set this in Unity Editor (Cards Holder)
    public RectTransform cards_holder;
    public RectTransform reserve_card_counter;
    public RectTransform card_sum_holder;
    public RectTransform tokens_holder;


    public RectTransform next_level;
    public Texture locked_next_level;
    public Texture unlocked_next_level;

    private RectTransform[] token_indicators = new RectTransform[4];
    private RawImage[] cards = new RawImage[8];
    private Player player;

    private void prepareTextures() {
        RawImage card1 = cards_holder.GetChild(0).GetComponent<RawImage>();
        RawImage card2 = cards_holder.GetChild(1).GetComponent<RawImage>();
        RawImage card3 = cards_holder.GetChild(2).GetComponent<RawImage>();
        RawImage card4 = cards_holder.GetChild(3).GetComponent<RawImage>();
        RawImage card5 = cards_holder.GetChild(4).GetComponent<RawImage>();
        RawImage card6 = cards_holder.GetChild(5).GetComponent<RawImage>();
        RawImage card7 = cards_holder.GetChild(6).GetComponent<RawImage>();
        RawImage card8 = cards_holder.GetChild(7).GetComponent<RawImage>();
        cards[0] = card1;
        cards[1] = card2;
        cards[2] = card3;
        cards[3] = card4;
        cards[4] = card5;
        cards[5] = card6;
        cards[6] = card7;
        cards[7] = card8;
    }

    private void prepareRanks() {
        texture_ranks = new List<char>();
        texture_ranks.Add('A');
        texture_ranks.Add('2');
        texture_ranks.Add('3');
        texture_ranks.Add('4');
        texture_ranks.Add('5');
        texture_ranks.Add('6');
        texture_ranks.Add('7');
        texture_ranks.Add('8');
        texture_ranks.Add('9');
        texture_ranks.Add('0');
        texture_ranks.Add('J');
        texture_ranks.Add('Q');
        texture_ranks.Add('K');
    }

    private void prepareTokenIndicators() {
        token_indicators[0] = tokens_holder.GetChild(0).GetComponent<RectTransform>();
        token_indicators[1] = tokens_holder.GetChild(1).GetComponent<RectTransform>();
        token_indicators[2] = tokens_holder.GetChild(2).GetComponent<RectTransform>();
        token_indicators[3] = tokens_holder.GetChild(3).GetComponent<RectTransform>();
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    private void setUIControl() {
        prepareTextures();
        prepareRanks();
        prepareTokenIndicators();
        isUIControlSet = true;
    }

    public void displayCardTexture(int card_position, char suite, char rank) {
        int rank_position = texture_ranks.FindIndex(d => d == rank);// transform rank character into index
        if (suite == 'H')
        {
            // hearts
            cards[card_position].texture = texture_hearts[rank_position];
        }
        else if (suite == 'D')
        {
            // diamonds
            cards[card_position].texture = texture_diamonds[rank_position];
        }
        else if (suite == 'C') {
            // clubs
            cards[card_position].texture = texture_clubs[rank_position];
        }
        else if (suite == 'S')
        {
            // spades
            cards[card_position].texture = texture_spades[rank_position];
        }
        else {
            Debug.LogError("Unknown Suite detected: " + suite);
        }
        cards[card_position].gameObject.SetActive(true); //set this card view active
    }

    public void displayCardTextures() {

        if (!isUIControlSet)
        {
            setUIControl();
        }

        disableTextures();// disable all textures
        //iterate player hand and enable cards as they are in list
        int i;
        for (i = 0; i < player.player_hand.current_hand_size; i++) {
            Card card = player.player_hand.current_hands[i];

            displayCardTexture(i, card.getSuite(), card.getRank());
        }
    }

    private void disableTextures() {
        int i;
        for (i = 0; i < cards.Length; i++) {
            cards[i].texture = null;
            cards[i].gameObject.SetActive(false);
        }
    }

    public void hideCardTexture(int current_hand_size) {
        int card_index = current_hand_size - 1;
        cards[card_index].gameObject.SetActive(false);
    }

    public void switchCardsView() {
        if (cards_holder.gameObject.activeInHierarchy)
        {
            cards_holder.gameObject.SetActive(false);
        }
        else {
            cards_holder.gameObject.SetActive(true);
        }
    }

    public void updateReserveCounter(int value) {
        string str_value = value.ToString();
        reserve_card_counter.GetComponent<Text>().text = str_value;
    }

    public void updateSumCounter(int sum) {
        card_sum_holder.GetComponent<Text>().text = sum.ToString();
        Color32 color;
        if (sum == 21)
        {
            // green colour
            color = new Color32(77, 251, 63, 255);
        }
        else {
            //white colour
            color = new Color32(255, 255, 255, 255);
        }
        card_sum_holder.GetComponent<Text>().color = color;
    }

    public void colourTokens(int counter, int polarity) {
        Color32 color;

        Debug.Log("at counter " + counter + " polarity is: " + polarity);

        if (polarity == 0)
        {
            // blue colour
            color = new Color32(52, 124, 255, 100);
        }
        else if (polarity == 1)
        {
            // orange colour
            color = new Color32(255, 187, 52, 100);
        }
        else if (polarity == 2)
        {
            // red colour
            color = new Color32(255, 79, 52, 100);
        }
        else if (polarity == 3)
        {
            // green colour
            color = new Color32(67, 255, 52, 100);
        }
        else {
            Debug.LogError("UIControls|colourTokens: unknown polarity");
            color = new Color32(255, 255, 255, 100);
        }

        Debug.Log("colour is: " + color);

        token_indicators[counter].GetComponent<Image>().color = color;
    }

    public void resetDefaultGUI() {
        //reset token indicators
        int i;
        foreach (RectTransform element in tokens_holder) {
            element.GetComponent<Image>().color = new Color32(255, 255, 255, 100);
        }

        //reset image of next_level
        next_level.GetComponent<RawImage>().texture = locked_next_level;
    }

    public void checkForNextLevel() {
        if (player.isNextLevelReady())
        {
            next_level.GetComponent<RawImage>().texture = unlocked_next_level;
        }
        else {
            next_level.GetComponent<RawImage>().texture = locked_next_level;
        }
    }

}
