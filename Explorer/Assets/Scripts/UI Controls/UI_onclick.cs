﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_onclick : MonoBehaviour, IPointerClickHandler {

    public void OnPointerClick(PointerEventData eventData)
    {
        string element_name = eventData.pointerCurrentRaycast.gameObject.name;
        Debug.Log("element name is: " + element_name);
        if (element_name.Contains("CardTexture"))
        {
            int ind = int.Parse(element_name.Substring(element_name.Length - 1)); //get index of card
            ind--;
            cardSelection(ind); //process this card
        }
        else if (element_name == "Card Sum")
        {
            Transform cards = GameObject.Find("Canvas").transform.GetChild(0); // 1st child should be Cards Panel
            if (cards.gameObject.activeInHierarchy)
            {
                cards.gameObject.SetActive(false);
            }
            else
            {
                cards.gameObject.SetActive(true);
            }
        }
        else if (element_name == "Card Discard")
        {
            // discard state is saved in player_hand component of player
            bool discardState = Camera.main.GetComponent<Board>().player.player_hand.discardState;
            if (!discardState)
            {
                eventData.pointerCurrentRaycast.gameObject.transform.parent.parent.GetComponent<Image>().color = new Color(255, 255, 255);
                Camera.main.GetComponent<Board>().player.player_hand.discardState = true;
            }
            else
            {
                eventData.pointerCurrentRaycast.gameObject.transform.parent.parent.GetComponent<Image>().color = new Color(0, 0, 0);
                Camera.main.GetComponent<Board>().player.player_hand.discardState = false;
            }
        }
        else if (element_name == "Card Reserve")
        {
            drawReserve();
        }
        else if (element_name == "Next Level") {
            goNextLevel();
        }
    }

    private void cardSelection(int card_index) {
        bool discardState = Camera.main.GetComponent<Board>().player.player_hand.discardState;
        if (discardState) {
            Camera.main.GetComponent<Board>().player.player_hand.dropCard(card_index);
        }
    }

    private void drawReserve() {
        //draw reserve card
        Camera.main.GetComponent<Board>().player.player_hand.drawReserveCard();
    }

    private void goNextLevel() {
        //check if conditions for going into next level are meet
        Player player = Camera.main.GetComponent<Board>().player;
        if (player.has_key && player.has_minimum_chips && player.checkForBlackjack()) {
            Camera.main.GetComponent<GameLogic>().startNewScene();
        }
    }
}
