﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand {

    public CrossSceneVariables cross_scene_variables;
    public UIControls ui_controls;
    public Deck deck;

    public List<Card> current_hands{ get; set; }
    private static readonly int MAX_HAND_SIZE = 8;
    public int current_hand_size { get; set; }

    public bool discardState { get; set; }

    private int reserve_card_count { get; set; }

    public Hand(Deck public_deck) {

        cross_scene_variables = GameObject.Find("SideScripts").GetComponent<CrossSceneVariables>();

        deck = public_deck;

        current_hands = new List<Card>();
        current_hand_size = 0;
        reserve_card_count = cross_scene_variables.reserve_card_count;

        discardState = false;
    }

    public int getMaxHandSize() { return MAX_HAND_SIZE; }
    public int getReserveCounter() { return reserve_card_count; }

    public void firstCardDraw() {
        drawCards(4); //initially, player draws 4 card
        ui_controls.updateReserveCounter(reserve_card_count); //update reserve card draw to initial value

        /*int i;
        for (i = 0; i < current_hands.Count; i++)
        {
            Debug.Log("player has card: " + current_hands[i].getSuite() + " " + current_hands[i].getRank());
        }*/
    }

    // Do not modify this function, modify _drawCard
    public void drawCards(int draw_count) {
        int i;
        for (i = 0; i < draw_count; i++) {
            _drawCard();
        }
        updateCurrentHandValue();
    }

    private void _drawCard() {
        Card draw = deck.drawTopCard();
        if (current_hands.Count < MAX_HAND_SIZE)
        {
            current_hands.Add(draw);
            current_hand_size++;

            ui_controls.displayCardTextures();
        }
        else {
            // ignore new cards, or create dialog and let user decide, which card to take
        }
        //Debug.Log("current_hand_size: " + current_hand_size);
    }

    public void dropCard(int card_index) {
        if (card_index < current_hand_size) {
            Card card = current_hands[card_index]; //select card
            deck.reuseCard(card); //reuse this card in deck
            current_hands.Remove(card); // remove from players hand
            current_hand_size--;

            ui_controls.displayCardTextures();

            updateCurrentHandValue();
        }
    }

    public void drawReserveCard() {
        if (reserve_card_count > 0) {
            drawCards(1);
            reserve_card_count--;

            ui_controls.updateReserveCounter(reserve_card_count);
        }
    }

    public void updateCurrentHandValue() {
        int sum = calculateCurrentHandValue();

        ui_controls.updateSumCounter(sum);
        ui_controls.checkForNextLevel();
    }

    public int calculateCurrentHandValue() {
        int i = 0;
        int sum = 0;
        int rank_index;
        for (; i < current_hand_size; i++)
        {
            rank_index = findRankIndex(current_hands[i].getRank());
            if (rank_index <= 9)
            {
                sum += (rank_index + 1);
            }
            else
            {
                sum += 10;
            }
        }
        return sum;
    }

    private int findRankIndex(char rank) {
        int i;
        for (i = 0; i < deck.ranks.Length; i++) {
            if (rank == deck.ranks[i]) {
                return i;
            }
        }
        Debug.LogError("Hand|findRankIndex: Rank is not inside deck.ranks - returning index -1.");
        return -1;
    }
}
