﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {


    public int current_node { get; set; }
    public int current_layer { get; set; }
    public Hand player_hand { get; set; }
    public Deck public_deck { get; set; }

    public bool has_key;
    public bool has_minimum_chips;

    public Player(Deck public_deck) {
        has_key = false;
        this.public_deck = public_deck;
        player_hand = new Hand(public_deck);
    }

    //prepare player for new area
    public void preparePlayer() {
        //place player in start_node
        current_layer = 0;
        current_node = 1;
    }

    public bool isNextLevelReady() {
        if (has_key && has_minimum_chips && checkForBlackjack())
        {
            return true;
        }
        else {
            return false;
        }
    }

    public bool checkForBlackjack() {
        if (player_hand.calculateCurrentHandValue() == 21)
        {
            return true;
        }
        else {
            return false;
        }
    }
}
