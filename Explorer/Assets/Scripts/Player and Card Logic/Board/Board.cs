﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {

    public Deck deck;
    public Player player;

    public int unlocked_token_counter;

    public void prepareBoard() {
        unlocked_token_counter = 0;
        deck = new Deck(); // create deck with initial full cards
        player = new Player(deck); // create player and instantiate him starting hand
        player.player_hand.ui_controls = GetComponent<UIControls>(); // pass down ui controls to hand script
        player.player_hand.ui_controls.setPlayer(player); // reverse-refference

        player.player_hand.firstCardDraw();
    }

    public void updateTokenIndicators(int polarity) {
        //update minimum_chips boolean
        if (!player.has_minimum_chips) {
            if (unlocked_token_counter == 3)
            {
                player.has_minimum_chips = true;
                player.player_hand.ui_controls.checkForNextLevel();
            }
        }

        if (unlocked_token_counter < 4) {
            //if we have first 4 tokens, colour the Token 1,... Token 4 in UI
            player.player_hand.ui_controls.colourTokens(unlocked_token_counter, polarity);
        }
        unlocked_token_counter = (unlocked_token_counter+1)%4;
    }

}
