﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck {

    private RandomGenerator random_generator;

    private List<Card> available_cards;
    private List<Card> used_cards { get; set; }

    public readonly char[] suites = { 'H', 'D', 'C', 'S' }; //Heart, Diamond, Clubs, Spaces
    public readonly char[] ranks = { 'A', '2', '3', '4', '5', '6', '7', '8', '9', '0','J', 'Q', 'K' };

    public Deck() {
        random_generator = GameObject.Find("SideScripts").GetComponent<RandomGenerator>();
        createInitialDeck();
    }

    public void createInitialDeck() {
        available_cards = new List<Card>();
        used_cards = new List<Card>();
        int i,j;
        for (i = 0; i < suites.Length; i++) {
            for (j = 0; j < ranks.Length; j++) {
                addCard( new Card(suites[i],ranks[j]) );
            }
        }
        random_generator.shuffleCards(ref available_cards); //shuffle cards
    }

    private void addCard(Card new_card)
    {
        // optionally, we could check if this is creaing a card duplicate (in-case)
        available_cards.Add(new_card);
    }

    private void removeCard(Card card_to_remove)
    {
        char suite = card_to_remove.getSuite();
        char rank = card_to_remove.getRank();

        foreach (var card in available_cards)
        {
            if (card.getRank() == rank)
            {
                if (card.getSuite() == suite)
                {
                    available_cards.Remove(card);
                    return;
                }
            }
        }
    }

    public bool isCardInDeck(char suite, char rank) {
        int i;
        for (i = 0; i < available_cards.Count; i++) {
            if (available_cards[i].getRank() == rank) {
                if (available_cards[i].getSuite() == suite) {
                    return true;
                }
            }
        }
        return false;
    }

    public Card drawTopCard() {
        if (available_cards.Count > 0)
        {
            Card draw = available_cards[0];
            available_cards.RemoveAt(0);
            return draw;
        }
        else {
            reuseCards(); //reuse cards, that arent in player hand now
            return drawTopCard(); //recursively draw top card
        }
    }

    private void reuseCards() {
        // move used cards to available cards
        int i;
        for (i = 0; i < used_cards.Count; i++) {
            available_cards.Add(used_cards[i]);
        }
        //clear used cards array
        used_cards = new List<Card>();
        
        //shuffle available cards
        random_generator.shuffleCards(ref available_cards);
    }

    public void reuseCard(Card card_for_reuse) {
        used_cards.Add(card_for_reuse);
    }
}
