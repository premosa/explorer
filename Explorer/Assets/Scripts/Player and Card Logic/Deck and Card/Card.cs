﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card {
    private char suite { get; set; }
    private char rank { get; set; }

    public Card(char new_suite, char new_rank) {
        suite = new_suite;
        rank = new_rank;
    }

    public char getRank() { return rank; }
    public char getSuite() { return suite; }
}
