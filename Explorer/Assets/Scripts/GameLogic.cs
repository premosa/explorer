﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour {

    public CrossSceneVariables cross_scene_variables;
    public RandomGenerator random_generator;
    public PhysicalObjects physical_objects;

    public GameObject play_area;

    //depth and width offset MUST be an EVEN number (both)
    public float depth_offset = 12f; //How much space is between 2 layers (transform-wise)
    public float width_offset = 12f; //How much space is between 2 nodes (transform-wise)

    public GameObject node_holder;
    public GameObject connection_holder;

    private GameObject[,] board; //physical board object

    //TODO: based on how far player has progressed, make this higher/lower
    private int difficulty = 1;
    public int layers = 6; //minimum is 4 layers
    public int nodes_per_layer = 5; // number of nodes per layer

    // Game Logic creates a new map of nodes, connects them and places objects on nodes
    void Start () {

        GameObject side_scripts = GameObject.Find("SideScripts");

        //set cross scene variables for gamelogic and player(s)
        cross_scene_variables = side_scripts.GetComponent<CrossSceneVariables>();

        // prepare physical objects
        physical_objects = side_scripts.GetComponent<PhysicalObjects>();
        physical_objects.preparePhysicalObjects();

        //prepare random generator
        random_generator = side_scripts.GetComponent<RandomGenerator>();

        //set reserve card count to 3 (initially)
        cross_scene_variables.reserve_card_count = 3;

        //prepare deck, hand, board, physical objects, gui actions and logic
        prepareGame();
    }

    private void prepareGame() {
        //prepare board script
        Camera.main.GetComponent<Board>().prepareBoard();

        // prepare variables
        prepareNodeWorldPositions();

        // prepare nodes
        prepareNodes(ref difficulty, ref layers);
        connectNodes();

        //spawn key
        randomizeKeyPosition();

        // replace placeholder objects with actual nodes and corridors
        replaceObjectModels();

        //set startNode as activateNode
        activateStartNode();
    }

    public void startNewScene() {
        //delete gameobjects of playarea, if it exists
        foreach (Transform child in play_area.transform) {
            Destroy(child.gameObject);
        }

        //in cross scene variables, update current reserve card counter
        cross_scene_variables.reserve_card_count = Camera.main.GetComponent<Board>().player.player_hand.getReserveCounter();
        cross_scene_variables.reserve_card_count++; //moving into next scene means we increment by 1

        //reset gui elements
        Camera.main.transform.GetComponent<UIControls>().resetDefaultGUI();

        //prepare game
        prepareGame();
    }

    private void prepareNodes(ref int difficulty, ref int layer_count) {

        //
        // Spawn nodes
        //

        board = new GameObject[layer_count, nodes_per_layer];

        //node position and name
        Vector3 pos;
        string name;
        
        //spawn 1 node in layer 1 (this is player spawn area, no items/events/enemies are here)
        {

            GameObject initial_node = Instantiate(node_holder);
            pos = new Vector3(0, 0, 0);
            name = "start_node";
            initial_node.GetComponent<Node>().prepareNewNode(ref pos, ref play_area, ref name, this);
            
            initial_node.GetComponent<Node>().setBoardPosition(0, 1);
            board[0,1] = initial_node;
        }

        // prepare node positions array
        List<int> prepare_node_position = new List<int>();
        int[] node_positions;
        {
            int c;
            for (c = 0; c < nodes_per_layer; c++) {
                prepare_node_position.Add(c);
            }
            node_positions = prepare_node_position.ToArray();
        }

        float layer_position = depth_offset; //move current_height to new layer
        int i, layer_node_count;
        for (i = 1; i < layer_count; i++) {
            if (i == layer_count - 1) {
                GameObject final_node = Instantiate(node_holder);
                pos = new Vector3(layer_position, 0, 0);
                name = "end_node";
                final_node.GetComponent<Node>().prepareNewNode(ref pos, ref play_area, ref name, this);
                
                final_node.GetComponent<Node>().setBoardPosition(layer_count - 1, 1);
                board[layer_count - 1, 1] = final_node;
            }
            else {
                layer_node_count = Random.Range(1, nodes_per_layer+1); // this means integers between [1,nodes_per_layer]
                random_generator.shuffleNodePositions(ref node_positions);
                int[] new_node_positions = new List<int>(node_positions).GetRange(0, layer_node_count).ToArray();
                spawnNodes(ref new_node_positions,ref layer_node_count, ref layer_position, ref i);
                layer_position += depth_offset; //finally, move current height in new layer
            }
        }
    }

    //int[] node_world_positions = { -2 * width_offset, -width_offset, 0, width_offset }; // where in world position is their Z-axis offset
    //int[] node_world_positions = { width_offset, 0, -width_offset, -2 * width_offset, -3 * width_offset }; // where in world position is their Z-axis offset. change with NODES_PER_LAYER
    float[] node_world_positions;
    private void spawnNodes(ref int[] positions, ref int node_count, ref float layer_position, ref int current_layer) {
        int i;
        for (i = 0; i < positions.Length; i++) {

            string new_name = "node_l" + current_layer + "n" + i;
            Vector3 pos = new Vector3(layer_position, 0, node_world_positions[positions[i]]);

            GameObject new_node = Instantiate(node_holder);
            new_node.GetComponent<Node>().prepareNewNode(ref pos, ref play_area, ref new_name, this);
            new_node.GetComponent<Node>().setBoardPosition(current_layer, positions[i]);

            board[current_layer, positions[i]] = new_node;
        }
    }

    private void prepareNodeWorldPositions() {
        List<float> new_node_world_positions = new List<float>();
        int i;
        float initial_width_offset = width_offset;
        for (i = 0; i < nodes_per_layer; i++) {
            new_node_world_positions.Add(initial_width_offset);
            initial_width_offset -= width_offset;
        }
        node_world_positions = new_node_world_positions.ToArray();
    }

    private void connectNodes() {
        //connect initial nodes
        {
            int i;
            for (i = 0; i < nodes_per_layer; i++)
            {
                if (board[1, i] != null)
                {
                    createNodeConnection(board[1, i], board[0, 1]);
                }
            }
        }

        // 2-way walkthrough algorithm
        // 1) go from start->finish and create connections
        {
            int layer, j, current_node_index, count_first_layer, count_second_layer;
            int[] nodes_current_layer, nodes_first_layer, nodes_second_layer;
            for (layer = 1; layer < layers - 2; layer++)
            {

                //get indices of connectable nodes
                nodes_current_layer = findNodesInLayer(layer);
                nodes_first_layer = findNodesInLayer(layer + 1);
                nodes_second_layer = findNodesInLayer(layer + 2);

                //iterate nodes of current layer
                for (current_node_index = 0; current_node_index < nodes_current_layer.Length; current_node_index++)
                {
                    //get current node
                    GameObject current_node = board[layer, nodes_current_layer[current_node_index]];

                    //shuffle nodes of first and second layer
                    random_generator.shuffleNodePositions(ref nodes_first_layer);
                    random_generator.shuffleNodePositions(ref nodes_second_layer);

                    //randomize, how many nodes we want to try connecting
                    count_first_layer = Random.Range(0, nodes_first_layer.Length + 1);
                    count_second_layer = Random.Range(0, nodes_second_layer.Length + 1);

                    //Try to connect nodes of layer 1 elements to current node
                    for (j = 0; j < count_first_layer; j++)
                    {
                        if (allowNodeConnection(current_node.GetComponent<Node>(), board[layer + 1, nodes_first_layer[j]].GetComponent<Node>()))
                        {
                            //Debug.Log("make connection! between" + current_node + "and " + board[layer + 1, nodes_first_layer[j]]);
                            createNodeConnection(board[layer + 1, nodes_first_layer[j]], board[layer, current_node.GetComponent<Node>().node_position]);
                        }
                    }

                    //try to connect nodes of layer 2 elements to current node
                    for (j = 0; j < count_second_layer; j++)
                    {
                        if (allowNodeConnection(current_node.GetComponent<Node>(), board[layer + 2, nodes_second_layer[j]].GetComponent<Node>()))
                        {
                            //Debug.Log("make connection! between " + current_node + " and " + board[layer + 2, nodes_second_layer[j]]);
                            createNodeConnection(board[layer + 2, nodes_second_layer[j]], board[layer, current_node.GetComponent<Node>().node_position]);
                        }
                    }
                }
            }
        }

        // 1) check if final node is connected
        if (!isNodeConnected(board[layers - 1, 1].GetComponent<Node>())) {
            reverseConnectNodeToLayer(board[layers - 1, 1].GetComponent<Node>(), layers - 2);
        }

        // 2) go from finish->start and to nodes without any connections, add new connections
        {
            int i;
            int current_layer = layers - 1;
            for (; current_layer >= 1; current_layer--) {

                int[] nodes = findNodesInLayer(current_layer);
                random_generator.shuffleNodePositions(ref nodes);

                for (i = 0; i < nodes.Length; i++) {
                    if (!isNodeConnected(board[current_layer, nodes[i]].GetComponent<Node>())) {
                        if (!reverseConnectNodeToLayer(board[current_layer, nodes[i]].GetComponent<Node>(), current_layer)) {
                            reverseConnectNodeToLayer(board[current_layer, nodes[i]].GetComponent<Node>(), current_layer - 1);
                        }
                    }
                }
            }
        }

        // 3) connect groups
        connectBoardGroups();

    }

    private void createNodeConnection(GameObject node_to, GameObject node_from) {
        GameObject new_connection = Instantiate(connection_holder);
        new_connection.transform.parent = node_from.transform;
        new_connection.GetComponent<Connection>().connectNodes(node_to, node_from, width_offset/2, depth_offset/2, ref board);
        
        node_from.GetComponent<Node>().addConnection(new_connection.GetComponent<Connection>());
    }

    private bool allowNodeConnection(Node from, Node to) {

        int layer_direction = (int)Mathf.Sign(to.layer_position - from.layer_position);
        int node_direction = (int)Mathf.Sign(to.node_position - from.node_position);
        
        int layer_movement = from.layer_position;
        int node_movement = from.node_position;
        bool moveVertical = false;

        //dont allow connection from same node to same node
        if (layer_movement == to.layer_position && node_movement == to.node_position) { return false; }

        while (true) {

            //change direction condition
            if (!moveVertical){
                if (node_movement == to.node_position) {
                    moveVertical = true;
                }
            }

            //move condition
            if (moveVertical) {
                layer_movement = layer_movement + layer_direction;
            }
            else {
                node_movement = node_movement + node_direction;
            }

            //end condition
            if (layer_movement == to.layer_position && node_movement == to.node_position)
            {
                return true;
            }

            //if we hit another node, we cannot connect desired nodes
            if (board[layer_movement, node_movement] != null) {
                if (board[layer_movement, node_movement].GetComponent<BoardElement>().type == 1) {
                    return false;
                }
            }
        }
    }

    private int[] findNodesInLayer(int l) {
        List<int> connectables = new List<int>();
        int i; for (i = 0; i < nodes_per_layer; i++) {
            if (board[l, i] != null) {
                if (board[l, i].GetComponent<BoardElement>().type == 1) {
                    connectables.Add(i);
                }
            }
        }
        int[] result = connectables.ToArray();
        return result;
    }

    private bool isNodeConnected(Node n) {
        // check if node is connected to left, bottom or right side
        if (n.left_element != null || n.right_element != null || n.top_element != null) { return true; }
        return false;
    }

    private bool reverseConnectNodeToLayer(Node to, int layer_from) {

        int[] nodes = findNodesInLayer(layer_from);
        random_generator.shuffleNodePositions(ref nodes);

        int layer_to = to.layer_position;
        int node_to = to.node_position;

        int i;
        GameObject f, t; //from, to gameobjects
        for (i = 0; i < nodes.Length; i++) {
            t = board[layer_to, node_to];
            f = board[layer_from, nodes[i]];
            if (allowNodeConnection(f.GetComponent<Node>(), t.GetComponent<Node>())) {
                //Debug.Log("reverse connect node: connect - from " + f + " to " + t);
                createNodeConnection(t, f); //connect 2 nodes
                return true; //we created a connection
            }
        }
        //Debug.Log("failed connecting node to a layer");
        return false; //we weren't able to create connection
    }

    private void connectBoardGroups() {
        int[,] board_groups;

        //all board_group are 0 at start
        int layer, node, group_counter;
        while (true) {

            group_counter = 1;
            board_groups = new int[layers, nodes_per_layer];

            for (layer = 0; layer < layers-1; layer++)
            {
                for (node = 0; node < nodes_per_layer; node++)
                {
                    if (board[layer, node] != null)
                    {
                        if (board[layer, node].GetComponent<BoardElement>().type == 1)
                        {
                            if (board_groups[layer, node] == 0)
                            {
                                expandNodeGroup(ref board_groups, group_counter, layer, node);
                                group_counter++;
                            }
                        }
                    }
                }
            }

            //Debug.Log("group_counter: " + group_counter);
            //when we have only 1 group, end loop
            if (group_counter == 2) { Debug.Log("GameLogic|connectBoardGroups:: all nodes are connected."); return; }
        }
    }

    private void expandNodeGroup(ref int[,] board_groups, int group_counter ,int seed_layer, int seed_node)
    {
        List<int> layer_list = new List<int>();
        List<int> node_list = new List<int>();

        // take seed. Set it's board_groups element to group counter. Then expand its neighbours into layer_list and node_list
        board_groups[seed_layer, seed_node] = group_counter;

        int bridge_layer = seed_layer;
        int bridge_node = seed_node;
        bool hasInitialNode = false;

        layer_list.Add(seed_layer);
        node_list.Add(seed_node);

        // expand neighbours, if they are not null elements, and their board_group is not yet set
        int l, n;
        BoardElement be;
        while (true) {
            if (layer_list.Count == 0) { break; } //when queue is empty, break

            // expand first element in queue
            l = layer_list[0];
            n = node_list[0];

            // set board_group as checked
            board_groups[l, n] = group_counter;

            be = board[l, n].GetComponent<BoardElement>();

            // check if we have initial node in this group
            if (l == 0 && n == 1) { hasInitialNode = true; }

            // check if this is perhaps the bottom most node, incase we need to connect to another group
            if (bridge_layer > l) {
                bridge_layer = l;
                bridge_node = n;
            }

            // try to expand neighbours
            // check left neighbour
            if (be.left_element != null) {
                if (board_groups[l, n - 1] == 0) {
                    layer_list.Add(l);
                    node_list.Add(n-1);
                }
            }

            // check top neighbour
            if (be.top_element != null) {
                if (board_groups[l + 1, n] == 0) {
                    layer_list.Add(l + 1);
                    node_list.Add(n);
                }
            }

            // check right element
            if (be.right_element != null) {
                if (board_groups[l, n + 1] == 0) {
                    layer_list.Add(l);
                    node_list.Add(n + 1);
                }
            }

            //check bottom element
            if (be.bottom_element != null) {
                if (board_groups[l - 1, n] == 0) {
                    layer_list.Add(l - 1);
                    node_list.Add(n);
                }
            }

            layer_list.RemoveAt(0);
            node_list.RemoveAt(0);

        }

        if (!hasInitialNode) {
            //do reverseConnection from board[bridge_layer,bridge_node]
            Node n_to = board[bridge_layer, bridge_node].GetComponent<Node>();
            if (!reverseConnectNodeToLayer(n_to, bridge_layer - 1)) {
                reverseConnectNodeToLayer(n_to, bridge_layer - 2);
            }
        }
    }

    private void replaceObjectModels() {
        int i, j;
        for (i = 0; i < layers; i++) {
            for (j = 0; j < nodes_per_layer; j++) {
                if (board[i, j] != null) {
                    if (board[i, j].GetComponent<BoardElement>().type == 1)
                    {
                        // Node
                        board[i, j].GetComponent<Node>().setPhysicalObject();
                    }
                    else if (board[i, j].GetComponent<BoardElement>().type == 2) {
                        // Crossroads
                        board[i, j].GetComponent<CrossRoad>().setPhysicalObject();
                    }
                }
            }
        }
    }

    private void activateStartNode() {
        board[0, 1].GetComponent<Node>().activateElement();
        board[0, 1].GetComponent<Node>().handleUserClick();
    }

    public List<ActiveNodes> returnListOfActiveNodes() {
        //iterate physical board and return list of active nodes
        List<ActiveNodes> active_nodes = new List<ActiveNodes>();
        int layer,node;
        for (layer = 0; layer < layers; layer++) {
            for (node = 0; node < nodes_per_layer; node++) {
                if (board[layer, node] != null) {
                    if (board[layer, node].GetComponent<BoardElement>().type == 1) {
                        if (board[layer, node].GetComponent<Node>().active_status == 1) {
                            ActiveNodes new_active_node = new ActiveNodes(layer,node);
                            active_nodes.Add(new_active_node);
                        }
                    }
                }
            }
        }
        return active_nodes;
    }

    public Node returnNodeOfPosition(int layer, int node) {
        return board[layer, node].GetComponent<Node>();
    }

    private void randomizeKeyPosition() {
        int current_layer, current_node;
        while (true) {
            current_layer = Random.Range(1,layers); //cannot spawn on layer 0
            current_node = Random.Range(0,nodes_per_layer);

            if (board[current_layer, current_node] != null) {
                if (board[current_layer, current_node].GetComponent<BoardElement>().type == 1) {
                    //we have a node, spawn key here
                    GameObject node_gameobject = board[current_layer,current_node];
                    GameObject new_key = Instantiate(physical_objects.key);
                    new_key.transform.name = "key";
                    new_key.transform.parent = node_gameobject.transform;
                    new_key.transform.position = new Vector3(node_gameobject.transform.position.x, new_key.transform.position.y, node_gameobject.transform.position.z);

                    return; //end function
                }
            }
        }
    }
}