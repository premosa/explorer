﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGenerator : MonoBehaviour{

    // https://stackoverflow.com/questions/273313/randomize-a-listt
    private static System.Random rng = new System.Random();
    public void shuffleNodePositions(ref int[] positions)
    {
        int n = positions.Length;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            int value = positions[k];
            positions[k] = positions[n];
            positions[n] = value;
        }
    }

    public void shuffleCards(ref List<Card> cards)
    {
        int n = cards.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            Card card = cards[k];
            cards[k] = cards[n];
            cards[n] = card;
        }
    }

}
