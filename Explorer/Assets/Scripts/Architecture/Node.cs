﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : BoardElement {

    private GameLogic game_logic;

    private Token token;
    private PhysicalObjects physical_objects_holder;
    private Board board;

    public Node(){
        type = 1;
    }


    public void prepareNewNode(ref Vector3 pos, ref GameObject new_parent, ref string new_name, GameLogic game_logic) {

        transform.position = pos;
        transform.parent = new_parent.transform;
        transform.name = new_name;
        connections = new List<Connection>();

        physical_objects_holder = GameObject.Find("SideScripts").GetComponent<PhysicalObjects>();

        board = Camera.main.GetComponent<Board>();
    }

    public void addConnection(Connection connection_to) {
        connections.Add(connection_to);
    }

    public override void setPhysicalObject() {

        GameObject node_placeholder = physical_objects_holder.node_placeholder;
        if (physical_object.name == node_placeholder.name) {
                                                 //replace default object model with modified one, based on right,bottom,left,top connection
                                                 /* simple coding  - 0000
                                                  * +1 if Right (R)
                                                  * +2 if Bottom (B)
                                                  * +4 if Left (L)
                                                  * +8 if Top (T)
                                                  * value 0 should not exist, since every node should always be connected
                                                  */

            int value = 0;
            if (top_element != null)
            {
                value += 1;
            }
            if (right_element != null) {
                value += 2;
            }
            if (bottom_element != null) {
                value += 4;
            }
            if (left_element != null) {
                value += 8;
            }

            //remove previous physical gameobject
            //save refference to transform.position and transform.parent of previous gameobject, then remove that placeholder
            Transform previous_object_parent = physical_object.transform.parent;
            Vector3 previous_object_position = physical_object.transform.position;
            Destroy(physical_object);

            //based on value choice, replace physical object with proper node selection
            if (value == 0)
            {
                Debug.Log("Nodes-setPhysicalObject: value is 0, which means no connections to this node. This is an error");
            }
            else {
                // prepare physical_object_refference script
                /*if (physical_objects_holder.node_physical_objects.Count == 0) {
                    physical_objects_holder.prepareNodePhysicalObjects();
                }*/
                GameObject node_physical_object_prefab = physical_objects_holder.node_physical_objects[value];

                GameObject new_physical_object = Instantiate(node_physical_object_prefab);
                new_physical_object.transform.parent = previous_object_parent;
                new_physical_object.transform.position = previous_object_position;
                physical_object = new_physical_object;

                //add node lock above physical object (when nodes are created, by default they should be locked)
                addNodeLockObject();
            }
        }
    }

    private void addNodeLockObject() {
        GameObject new_node_lock = Instantiate(physical_objects_holder.node_lock);

        new_node_lock.name = "node_lock";
        new_node_lock.transform.parent = physical_object.transform.parent;
        new_node_lock.transform.position = new_node_lock.transform.position + physical_object.transform.position;
    }

    public void handleUserClick() {
        if (active_status == 0)
        {
            Debug.Log("Node|handleUserClick::Node is locked... click node, which is available");
        }
        else if (active_status == 1)
        {
            //Board board = Camera.main.GetComponent<Board>();
            active_status = 2; //node was cleared
            checkForKey();
            tokenAction(board);
            recursiveNodeActivation(); //activate adjacent nodes
        }
        else if (active_status == 2) {
            Debug.Log("Node|handleUserClick::Node has already been cleared...");
        }
    }

    private void tokenAction(Board board) {
        token.activateToken(board); //activate tokens ability
        board.updateTokenIndicators(token.polarity);
        destroyToken();
    }

    public override void activateElement() {
        // set active status to 1
        active_status = 1;

        //create token inside node
        createToken();
        
        // find node physical object
        int searched_index = findChildBySubstring(transform,"Node(Clone)");
        if (searched_index >= 0) {
            // turn on lights objects in node (first 2 child objects of physical node)
            transform.GetChild(searched_index).GetChild(0).gameObject.SetActive(true);
            transform.GetChild(searched_index).GetChild(1).gameObject.SetActive(true);
        }

        //destroy node_lock, if node has it
        searched_index = findChildBySubstring(transform,"node_lock");
        if (searched_index >= 0) {
            Destroy(transform.GetChild(searched_index).gameObject);
        }
    }

    private void createToken() {
        Token new_token;
        if (layer_position == 0 && node_position == 1)
        {
            //create "dummy" token for start_node, polarity = 0
            new_token = randomizeToken(0);
        }
        else {
            // determine which token we spawn
            int polarity = Random.Range(0, 101);
            polarity = determinePolarityPercentage(polarity);
            new_token = randomizeToken(polarity);
        }
        token = new_token;
    }

    private int determinePolarityPercentage(int polarity) {
        if (polarity < 10) {
            return 0; // dummy token - no effect
        }
        if (polarity < 52)
        {
            return 1; // positive token
        }
        else if (polarity < 100)
        {
            return 2; // negative token
        }
        else if (polarity == 100)
        {
            return 3; // polarity == 100, special token
        }
        else {
            Debug.LogError("Node|determinePolarityPercentage has polarity >100.");
            return -1;
        }
    }

    private Token randomizeToken(int polarity)
    {
        Deck deck = board.deck;
        if (polarity == 0)
        {

            //spawn a dummy token
            Token new_token = new DummyToken();
            new_token.physical_object = Instantiate(physical_objects_holder.dummy_token); // instantiate gameobject
            setTokenTransform(new_token);
            return new_token;
        }
        if (polarity == 1)
        {

            //randomize a positive token
            int pos_card_ind = Random.Range(0, PositiveToken.POSITIVE_TOKEN_COUNT);
            if (pos_card_ind == 0)
            {
                // positive card 1 - draw 1 card
                DrawCardToken new_token = new DrawCardToken();
                new_token.physical_object = Instantiate(physical_objects_holder.positive_tokens_physical_objects[pos_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
            else if (pos_card_ind == 1)
            {
                // positive card 2 - draw until X cards
                DrawUntilXCardsToken new_token = new DrawUntilXCardsToken();
                new_token.physical_object = Instantiate(physical_objects_holder.positive_tokens_physical_objects[pos_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
            else{
                // positive card 3 - draw 2 cards
                DrawTwoCardsToken new_token = new DrawTwoCardsToken();
                new_token.physical_object = Instantiate(physical_objects_holder.positive_tokens_physical_objects[pos_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
        }
        else if (polarity == 2)
        {

            //randomize a negative token
            int neg_card_ind = Random.Range(0, NegativeToken.NEGATIVE_TOKEN_COUNT);
            if (neg_card_ind == 0)
            {
                // negative card 1 - drop a random card
                dropCardToken new_token = new dropCardToken();
                new_token.physical_object = Instantiate(physical_objects_holder.negative_tokens_physical_objects[neg_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
            else if (neg_card_ind == 1)
            {
                // negative card 2 - reshuffle cards
                reshuffleCardsToken new_token = new reshuffleCardsToken();
                new_token.physical_object = Instantiate(physical_objects_holder.negative_tokens_physical_objects[neg_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
            else{
                // negative card 3 - drop all cards of type X
                int type_index = Random.Range(0, 4);
                char drop_type = deck.suites[type_index];
                dropCardTypeToken new_token = new dropCardTypeToken(drop_type);
                new_token.physical_object = Instantiate(physical_objects_holder.type_minuses[type_index]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
        }
        else if (polarity == 3)
        {
            //randomize a special token
            int special_card_ind = Random.Range(0, SpecialToken.SPECIAL_TOKEN_COUNT);
            if (special_card_ind == 0)
            {
                // special card 1 - reroll all tokens
                RerollTokensToken new_token = new RerollTokensToken();
                new_token.physical_object = Instantiate(physical_objects_holder.special_tokens_physical_objects[special_card_ind]); // instantiate gameobject
                setTokenTransform(new_token);
                return new_token;
            }
            else {
                Debug.LogError("Node|randomizeToken has polarity != 0, which is out of bounds.");
                return null;
            }
        }
        else {
            //unknown token type
            Debug.LogError("Node|randomizeToken has unknown polarity type.");
            return null;
        }
    }

    private void setTokenTransform(Token new_token)
    {
        new_token.physical_object.transform.position = new Vector3(transform.position.x, new_token.physical_object.transform.position.y, transform.position.z); // set transform position
        new_token.physical_object.transform.parent = transform; // this might need more changes
    }

    private void destroyToken() {
        Destroy(token.physical_object.gameObject); // destroy physical token and clear script refference
        token = null;
    }

    public void rerandomToken() {
        destroyToken(); //destroy token
        createToken(); // create a new token
    }

    private void checkForKey() {
        Transform key_transform = transform.Find("key");
        if (key_transform != null) {
            //we found key, delete it and set found_key to true
            board.player.has_key = true;

            Destroy(key_transform.gameObject);

            //check if we meet all conditions
            Camera.main.GetComponent<UIControls>().checkForNextLevel();
        }
    }
}
