﻿using System.Collections;
using System.Collections.Generic;

public class ActiveNodes
{
    public int layer;
    public int node;

    public ActiveNodes(int new_layer, int new_node)
    {
        layer = new_layer;
        node = new_node;
    }
}
