﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BoardElement : MonoBehaviour {

    public int active_status;
    /*
     Active status:
     0 - closed
     1 - open
     2 - cleared
         */

    public int type = 0;
    /*
     types:
      0 - none (undefined)
      1 - node / active area
      2 - crossroads
      */

    // BoardElement connections on board

    [HideInInspector] public BoardElement left_element;
    [HideInInspector] public BoardElement top_element;
    [HideInInspector] public BoardElement right_element;
    [HideInInspector] public BoardElement bottom_element;

    [HideInInspector] public int layer_position;
    [HideInInspector] public int node_position;

    public GameObject physical_object;
    public List<Connection> connections;

    //based on left,top, right and bottom element, instantiate proper gameobject model
    public abstract void setPhysicalObject();
    public abstract void activateElement();

    // Default constructor
    public BoardElement() {

        left_element = null;
        top_element = null;
        right_element = null;
        bottom_element = null;

        layer_position = -1;
        node_position = -1;

        physical_object = null;
        connections = new List<Connection>();
    }

    public void setBoardPosition(int new_layer_position, int new_node_position)
    {
        layer_position = new_layer_position;
        node_position = new_node_position;
    }

    private List<BoardElement> recursive_object_list = new List<BoardElement>();
    public void recursiveNodeActivation()
    {
        //recursive function
        if (top_element != null)
        {
            // top node
            if (top_element.type == 1)
            {
                Node top_node = top_element.GetComponent<Node>();
                if (top_node.active_status == 0)
                {
                    top_node.activateElement();
                }
            }
            // top crossroads
            else if (top_element.type == 2 && !recursive_object_list.Contains(top_element))
            {
                recursive_object_list.Add(top_element);
                top_element.GetComponent<CrossRoad>().recursiveNodeActivation();
                if (top_element.GetComponent<CrossRoad>().active_status == 0)
                {
                    //activate crossroads
                    top_element.GetComponent<CrossRoad>().activateElement();
                }
            }
        }

        if (right_element != null)
        {
            //right node
            if (right_element.type == 1)
            {
                Node right_node = right_element.GetComponent<Node>();
                if (right_node.active_status == 0)
                {
                    right_node.activateElement();
                }
            }
            //right crossroads
            else if (right_element.type == 2 && !recursive_object_list.Contains(right_element))
            {
                recursive_object_list.Add(right_element);
                right_element.GetComponent<CrossRoad>().recursiveNodeActivation();
                if (right_element.GetComponent<CrossRoad>().active_status == 0)
                {
                    right_element.GetComponent<CrossRoad>().activateElement();
                }
            }
        }

        if (bottom_element != null)
        {
            //bottom node
            if (bottom_element.type == 1)
            {
                Node bottom_node = bottom_element.GetComponent<Node>();
                if (bottom_node.active_status == 0)
                {
                    bottom_node.activateElement();
                }
            }
            //bottom crossroads
            else if (bottom_element.type == 2 && !recursive_object_list.Contains(bottom_element))
            {
                recursive_object_list.Add(bottom_element);
                bottom_element.GetComponent<CrossRoad>().recursiveNodeActivation();
                if (bottom_element.GetComponent<CrossRoad>().active_status == 0)
                {
                    bottom_element.GetComponent<CrossRoad>().activateElement();
                }
            }
        }

        if (left_element != null)
        {
            // left node
            if (left_element.type == 1)
            {
                Node left_node = left_element.GetComponent<Node>();
                if (left_node.active_status == 0)
                {
                    left_node.activateElement();
                }
            }
            // left crossroads
            else if (left_element.type == 2 && !recursive_object_list.Contains(left_element))
            {
                recursive_object_list.Add(left_element);
                left_element.GetComponent<CrossRoad>().recursiveNodeActivation();
                if (left_element.GetComponent<CrossRoad>().active_status == 0)
                {
                    left_element.GetComponent<CrossRoad>().activateElement();
                }
            }
        }
    }

    protected int findChildBySubstring(Transform custom_transform, string substring)
    {
        int i;
        //Debug.Log("transform t child count: " + custom_transform.childCount);
        for (i = 0; i < custom_transform.childCount; i++)
        {
            if (custom_transform.GetChild(i).name.Contains(substring))
            {
                return i;
            }
        }
        return -1; //exception case if we don't find
    }

    protected void destroyChildBySubstring(Transform custom_transform, string substring) {
        int searched_index = findChildBySubstring(custom_transform, substring);
        if (searched_index >= 0)
        {
            Destroy(custom_transform.GetChild(searched_index).gameObject);
        }
        else {
            Debug.LogError("BoardElement|destroyChildBySubstring: searched_index was outside of bounds!"); // exception
        }
    }

}
