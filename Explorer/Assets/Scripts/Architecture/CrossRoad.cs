﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossRoad : BoardElement {

    private PhysicalObjects physical_objects_holder;

    public CrossRoad() {
        type = 2;
    }

    public override void setPhysicalObject()
    {
        physical_objects_holder = GameObject.Find("SideScripts").GetComponent<PhysicalObjects>();
        GameObject crossroad_placeholder = physical_objects_holder.crossroad_placeholder;
        if (physical_object.name == crossroad_placeholder.name+"(Clone)")
        {

            //save refference to transform.position and transform.parent of previous gameobject, then remove that placeholder
            //remove previous physical gameobject

            Transform previous_object_parent = physical_object.transform.parent;
            Vector3 previous_object_position = physical_object.transform.position;
            Destroy(physical_object);

            GameObject new_crossroads_object = Instantiate(physical_objects_holder.crossroads);
            new_crossroads_object.transform.parent = previous_object_parent;
            new_crossroads_object.transform.position = previous_object_position;
            physical_object = new_crossroads_object;

            //remove passages from physical object, if they do not connect any nodes/crossroads
            initialCrossroadModification();

        }
    }

    public override void activateElement() {
        //set active status to 2 (crossroads get cleared right away; there is no state 1)
        active_status = 2;

        // find node physical object
        int searched_index = findChildBySubstring(transform,"crossroads(Clone)");
        if (searched_index >= 0)
        {
            // turn on lights objects in crossroads (first child object of physical crossroads)
            transform.GetChild(searched_index).GetChild(0).gameObject.SetActive(true);
            //clean-up the rest of corridor physical gameobjects for proper display
            cleanCorridor(transform.GetChild(searched_index).transform);
        }
    }

    private void initialCrossroadModification() {

        Transform phys_transform = physical_object.transform;
        if (top_element == null)
        {
            destroyChildBySubstring(phys_transform, "passage_top_ceil");
            destroyChildBySubstring(phys_transform, "passage_top_floor");
        }

        if (right_element == null)
        {
            destroyChildBySubstring(phys_transform, "passage_right_ceil");
            destroyChildBySubstring(phys_transform, "passage_right_floor");
        }

        if (bottom_element == null)
        {
            destroyChildBySubstring(phys_transform, "passage_bottom_ceil");
            destroyChildBySubstring(phys_transform, "passage_bottom_floor");
        }

        if (left_element == null) {
            destroyChildBySubstring(phys_transform, "passage_left_ceil");
            destroyChildBySubstring(phys_transform, "passage_left_floor");
        }

    }

    private void cleanCorridor(Transform phys_transform) {
        //destroy crossroads middle ceil element
        destroyChildBySubstring(phys_transform, "crossroads_ceil");

        if (top_element != null)
        {
            destroyChildBySubstring(phys_transform, "crossroads_door_top");
            destroyChildBySubstring(phys_transform, "passage_top_ceil");
        }

        if (right_element != null)
        {
            destroyChildBySubstring(phys_transform, "crossroads_door_right");
            destroyChildBySubstring(phys_transform, "passage_right_ceil");
        }

        if (bottom_element != null)
        {
            destroyChildBySubstring(phys_transform, "crossroads_door_bottom");
            destroyChildBySubstring(phys_transform, "passage_bottom_ceil");
        }

        if (left_element != null)
        {
            destroyChildBySubstring(phys_transform, "crossroads_door_left");
            destroyChildBySubstring(phys_transform, "passage_left_ceil");
        }

    }

}
