﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connection: MonoBehaviour {

    //public GameObject corridor;
    public GameObject crossroad;

    [HideInInspector] public GameObject object_from;
    [HideInInspector] public GameObject object_to;

    public void connectNodes(GameObject nto, GameObject nfrom, float width_offset, float depth_offset, ref GameObject[,] board) {

        //Debug.Log("Connection||connectNodes:: object from: " + nfrom.name + ", object to: " + nto.name);

        // width_offset and depth_offset are half the distance of width&depth_offset of GameLogic, since then they are placed right inbetween as a corridor between 2 nodes
        // Also the reason why depth_offset and width_offset must be EVEN numbers in GameLogic (else they would have to be float, making unnecesarry harder instructions)

        object_from = nfrom;
        object_to = nto;

        Node ns_from = nfrom.GetComponent<Node>();
        Node ns_to = nto.GetComponent<Node>();

        int width = ns_from.node_position - ns_to.node_position; // width is diffrence between source-target nodes
        int depth = ns_to.layer_position - ns_from.layer_position; // depth is diffrence between source-target layers

        int l, n;
        // l,n are "layer" and "node" of movement direction - 
        //if-case for width
        if (width == 0)
        {
            if (depth >= 1)
            {

                //up
                upTurn(ns_from, ref board, ref depth_offset);

                if (depth >= 2)
                {
                    //up up
                    l = ns_from.layer_position + 1;
                    n = ns_from.node_position;
                    GameObject element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);
                }
            }
        }
        else if (width == 1)
        {
            if (depth >= 0)
            {

                //left
                leftTurn(ns_from, ref board, ref width_offset);

                if (depth >= 1)
                {

                    //left up
                    l = ns_from.layer_position;
                    n = ns_from.node_position - 1;
                    GameObject element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);

                    if (depth >= 2)
                    {

                        //left up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);
                    }
                }
            }
        }
        else if (width == 2)
        {
            if (depth >= 0)
            {
                // left left
                leftTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position - 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {

                    //left left up
                    n = n - 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);

                    if (depth >= 2)
                    {

                        //left left up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);
                    }
                }
            }
        }
        else if (width == 3)
        {
            if (depth >= 0)
            {
                // left left left
                leftTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position - 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n - 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {

                    //left left left up
                    n = n - 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                    if (depth >= 2)
                    {
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);
                    }
                }
            }
        }
        else if (width == 4)
        {
            if (depth >= 0)
            {
                // left left left left
                leftTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position - 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n - 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n - 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                leftTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {
                    //left left left left up
                    n = n - 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                    if (depth >= 2)
                    {
                        //left left left left up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);
                    }
                }
            }
        }
        else if (width == -1)
        {
            if (depth >= 0)
            {
                //right
                rightTurn(ns_from, ref board, ref width_offset);

                if (depth >= 1)
                {
                    //right up
                    l = ns_from.layer_position;
                    n = ns_from.node_position + 1;
                    GameObject element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);

                    if (depth >= 2)
                    {
                        // right up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref depth_offset);
                    }
                }
            }
        }
        else if (width == -2)
        {
            if (depth >= 0)
            {
                //right right
                rightTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position + 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {
                    //right right up
                    n = n + 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                    if (depth >= 2)
                    {
                        //right right up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);
                    }
                }
            }
        }
        else if (width == -3)
        {
            if (depth >= 0)
            {
                //right right right
                rightTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position + 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n + 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {
                    // right right right up
                    n = n + 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                    if (depth >= 2)
                    {
                        //right right right up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);
                    }
                }
            }
        }
        else if (width == -4) {
            if (depth >= 0) {
                //right right right right
                rightTurn(ns_from, ref board, ref width_offset);

                l = ns_from.layer_position;
                n = ns_from.node_position + 1;
                GameObject element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n + 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                n = n + 1;
                element = board[l, n];
                element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                rightTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                if (depth >= 1)
                {
                    // right right right up
                    n = n + 1;
                    element = board[l, n];
                    element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                    upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);

                    if (depth >= 2)
                    {
                        //right right right up up
                        l = l + 1;
                        element = board[l, n];
                        element.GetComponent<CrossRoad>().setBoardPosition(l, n);
                        upTurn(element.GetComponent<CrossRoad>(), ref board, ref width_offset);
                    }
                }
            }
        }
    }

    //TODO: remove this; since crossways and nodes are next to eachother, corridors are not necesarry
    /*public void createNewCorridor(Transform object_transform,float depth,float width) {
        GameObject new_corridor = Instantiate(corridor);

        new_corridor.transform.parent = object_transform; //this is object of this script is parent of physical corridor
        new_corridor.transform.position = object_transform.transform.position + new Vector3(depth, 0, width);
    }*/

    public GameObject createNewCrossroad(Transform object_transform, int depth, int width, int layer_index, int node_index, ref GameObject[,] board) {
        GameObject new_crossroad = Instantiate(crossroad);
        GameObject crossroad_physical_object = Instantiate(crossroad.GetComponent<CrossRoad>().physical_object);

        crossroad_physical_object.transform.parent = new_crossroad.transform;
        crossroad_physical_object.transform.position = new Vector3(0, 0, 0);

        new_crossroad.transform.parent = object_transform;
        board[layer_index, node_index] = new_crossroad;
        new_crossroad.transform.position = object_transform.position + new Vector3(depth, 0, width);

        return new_crossroad;
    }

    private bool checkIfBoardOccupied(int layer, int node, ref GameObject[,] board) {
        if (board[layer, node] == null) { return false; }
        return true;
    }

    private void leftTurn(BoardElement be_from, ref GameObject[,] board, ref float width_offset) {
        int l = be_from.layer_position;
        int n = be_from.node_position - 1;
        if (!checkIfBoardOccupied(l, n, ref board))
        {
            //board is not occupied
            GameObject new_crossroad = Instantiate(crossroad);
            GameObject crossroad_physical_object = Instantiate(crossroad.GetComponent<CrossRoad>().physical_object);

            new_crossroad.GetComponent<CrossRoad>().physical_object = crossroad_physical_object;

            crossroad_physical_object.transform.parent = new_crossroad.transform;

            //createNewCorridor(be_from.transform, 0, width_offset);

            new_crossroad.transform.parent = be_from.transform;
            board[l, n] = new_crossroad;
            new_crossroad.transform.position = be_from.transform.position + new Vector3(0, 0, width_offset * 2);

            be_from.left_element = new_crossroad.GetComponent<CrossRoad>();
            new_crossroad.GetComponent<CrossRoad>().right_element = be_from;
        }
        else if (board[l, n].GetComponent<BoardElement>().type == 1)
        {
            //createNewCorridor(be_from.transform, 0, width_offset);

            //board element is node, make connection between them
            be_from.left_element = board[l, n].GetComponent<Node>();
            board[l, n].GetComponent<Node>().right_element = be_from;
        }
        else if (board[l, n].GetComponent<BoardElement>().type == 2) {
            //board element is a crossroad - make connections
            CrossRoad cr = board[l, n].GetComponent<CrossRoad>();
            if (cr.right_element == null)
            {
                //createNewCorridor(be_from.transform, 0, width_offset);

                cr.right_element = be_from;
                be_from.left_element = cr;
            }
            else {
                //Debug.Log("Connection|leftTurn:: Corridor on the right.");
            }
        }
    }

    private void rightTurn(BoardElement be_from, ref GameObject[,] board, ref float width_offset)
    {
        int l = be_from.layer_position;
        int n = be_from.node_position + 1;
        if (!checkIfBoardOccupied(l, n, ref board))
        {
            //board is not occupied
            GameObject new_crossroad = Instantiate(crossroad);
            GameObject crossroad_physical_object = Instantiate(crossroad.GetComponent<CrossRoad>().physical_object);

            new_crossroad.GetComponent<CrossRoad>().physical_object = crossroad_physical_object;

            //createNewCorridor(be_from.transform, 0, -width_offset);

            crossroad_physical_object.transform.parent = new_crossroad.transform;

            new_crossroad.transform.parent = be_from.transform;
            board[l, n] = new_crossroad;
            new_crossroad.transform.position = be_from.transform.position + new Vector3(0, 0, -2 * width_offset);

            be_from.right_element = new_crossroad.GetComponent<CrossRoad>();
            new_crossroad.GetComponent<CrossRoad>().left_element = be_from;
        }
        else if (board[l, n].GetComponent<BoardElement>().type == 1)
        {
            //createNewCorridor(be_from.transform, 0, -width_offset);

            //board element is node, make connection between them
            be_from.right_element = board[l, n].GetComponent<Node>();
            board[l, n].GetComponent<Node>().left_element = be_from;
        }
        else if (board[l, n].GetComponent<BoardElement>().type == 2)
        {
            //board element is a crossroad - make connections
            CrossRoad cr = board[l, n].GetComponent<CrossRoad>();
            if (cr.left_element == null)
            {
                cr.left_element = be_from;
                be_from.right_element = cr;

                //createNewCorridor(be_from.transform, 0, -width_offset);
            }
            else
            {
                //Debug.Log("Connection|rightTurn:: Corridor on the left.");
            }
        }
    }

    private void upTurn(BoardElement be_from, ref GameObject[,] board, ref float depth_offset)
    {
        int l = be_from.layer_position + 1;
        int n = be_from.node_position;
        if (!checkIfBoardOccupied(l, n, ref board))
        {
            //board is not occupied
            GameObject new_crossroad = Instantiate(crossroad);
            GameObject crossroad_physical_object = Instantiate(crossroad.GetComponent<CrossRoad>().physical_object);

            new_crossroad.GetComponent<CrossRoad>().physical_object = crossroad_physical_object;

            //createNewCorridor(be_from.transform, depth_offset, 0);

            crossroad_physical_object.transform.parent = new_crossroad.transform;

            new_crossroad.transform.parent = be_from.transform;
            board[l, n] = new_crossroad;
            new_crossroad.transform.position = be_from.transform.position + new Vector3(2 * depth_offset, 0, 0);

            be_from.top_element= new_crossroad.GetComponent<CrossRoad>();
            new_crossroad.GetComponent<CrossRoad>().bottom_element = be_from;

        }
        else if (board[l, n].GetComponent<BoardElement>().type == 1)
        {
            //createNewCorridor(be_from.transform, depth_offset, 0);

            //board element is node, make connection between them
            be_from.top_element = board[l, n].GetComponent<Node>();
            board[l, n].GetComponent<Node>().bottom_element= be_from;

        }
        else if (board[l, n].GetComponent<BoardElement>().type == 2)
        {
            //board element is a crossroad - make connections
            CrossRoad cr = board[l, n].GetComponent<CrossRoad>();
            if (cr.bottom_element == null)
            {
                //createNewCorridor(be_from.transform, depth_offset, 0);

                cr.bottom_element = be_from;
                be_from.top_element = cr;
            }
            else
            {
                Debug.Log("Error! Connections-LeftTurn: Trying to make right element, but space is already taken.");
            }
        }
    }
}
