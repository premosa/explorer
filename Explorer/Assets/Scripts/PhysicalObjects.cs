﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PhysicalObjects : MonoBehaviour {

    public GameObject node_placeholder;
    public GameObject crossroad_placeholder;

    public GameObject node_lock;
    public GameObject crossroads;

    public GameObject key;

    public GameObject T_node;
    public GameObject TR_node;
    public GameObject TB_node;
    public GameObject TL_node;
    public GameObject TRB_node;
    public GameObject TRL_node;
    public GameObject TBL_node;
    public GameObject TRBL_node;
    public GameObject R_node;
    public GameObject RB_node;
    public GameObject RL_node;
    public GameObject RBL_node;
    public GameObject B_node;
    public GameObject BL_node;
    public GameObject L_node;

    [HideInInspector] public List<GameObject> node_physical_objects;

    public void preparePhysicalObjects() {
        prepareNodePhysicalObjects();
        prepareTokenPhysicalObjects();
    }

    private void prepareNodePhysicalObjects() {
        node_physical_objects = new List<GameObject>();

        node_physical_objects.Add(null); //0, error
        node_physical_objects.Add(R_node); // 1
        node_physical_objects.Add(B_node); // 2
        node_physical_objects.Add(RB_node); // 3
        node_physical_objects.Add(L_node); // 4
        node_physical_objects.Add(RL_node); // 5
        node_physical_objects.Add(BL_node); // 6
        node_physical_objects.Add(RBL_node); // 7
        node_physical_objects.Add(T_node); // 8
        node_physical_objects.Add(TR_node); // 9
        node_physical_objects.Add(TB_node); // 10
        node_physical_objects.Add(TRB_node); // 11
        node_physical_objects.Add(TL_node); // 12
        node_physical_objects.Add(TRL_node); // 13
        node_physical_objects.Add(TBL_node); // 14
        node_physical_objects.Add(TRBL_node); // 15
    }

    [HideInInspector] public List<GameObject> positive_tokens_physical_objects;
    public GameObject draw_card_token;
    public GameObject until_X_card_token;
    public GameObject draw_two_cards_token;

    [HideInInspector] public List<GameObject> negative_tokens_physical_objects;
    public GameObject drop_random_card_token;
    public GameObject randomize_hand_token;

    [HideInInspector] public List<GameObject> special_tokens_physical_objects;
    public GameObject reroll_tokens_token;

    public GameObject dummy_token;

    public GameObject type_minus_hearts;
    public GameObject type_minus_spades;
    public GameObject type_minus_clubs;
    public GameObject type_minus_diamonds;
    [HideInInspector] public GameObject[] type_minuses;

    private void prepareTokenPhysicalObjects() {
        positive_tokens_physical_objects = new List<GameObject>();
        positive_tokens_physical_objects.Add(draw_card_token);
        positive_tokens_physical_objects.Add(until_X_card_token);
        positive_tokens_physical_objects.Add(draw_two_cards_token);

        negative_tokens_physical_objects = new List<GameObject>();
        negative_tokens_physical_objects.Add(drop_random_card_token);
        negative_tokens_physical_objects.Add(randomize_hand_token);
        negative_tokens_physical_objects.Add(null); //this index belongs to type_minuses array
        type_minuses = new GameObject[4];
        type_minuses[0] = type_minus_hearts;
        type_minuses[1] = type_minus_diamonds;
        type_minuses[2] = type_minus_clubs;
        type_minuses[3] = type_minus_spades;

        special_tokens_physical_objects = new List<GameObject>();
        special_tokens_physical_objects.Add(reroll_tokens_token);
    }
}
