﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{

    // Source (modified): https://kylewbanks.com/blog/unity3d-panning-and-pinch-to-zoom-camera-with-touch-and-mouse-input

    private static readonly float PanSpeed = 30f;
    private bool click_status;
    private float click_duration;
    private static readonly float max_click_duration = 0.1f;
    private static readonly float max_click_distance_offset = 0.05f;


    private float[] BoundsX = new float[] { 0f, 0f };
    private float[] BoundsZ = new float[] { 0f, 0f };

    private Camera cam;

    private Vector3 lastPanPosition;
    private int panFingerId; // Touch mode only

    private bool wasZoomingLastFrame; // Touch mode only
    private Vector2[] lastZoomPositions; // Touch mode only

    private Quaternion lastRotation;

    private GameLogic GL;

    void Awake()
    {
        // set/get components
        cam = Camera.main; //get main camera
        GL = cam.GetComponent<GameLogic>();

        //set Z bounds
        BoundsZ[0] = -GL.width_offset * (GL.nodes_per_layer - 2) - (GL.width_offset/2);
        BoundsZ[1] = GL.width_offset;

        //set X bounds
        BoundsX[0] = -GL.depth_offset;
        BoundsX[1] = GL.depth_offset * (GL.layers - 2) + (GL.depth_offset/2);

        //set default click values
        click_status = false;
        click_duration = 0;
    }

    void LateUpdate()
    {
        // TODO: uncomment this when menus is implemented
        //if (isMenuOpen)
        //{
        //    return;
        //}

        if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer)
        {
            HandleTouch();
        }
        else
        {
            HandleMouse();
        }
    }

    void HandleTouch()
    {

    }

    void HandleMouse()
    {
        // On mouse down, capture it's position.
        // Otherwise, if the mouse is still down, pan the camera.
        if (Input.GetMouseButtonDown(0))
        {
            lastPanPosition = Input.mousePosition; // in pixel coordinates
            click_status = true;
            click_duration = 0;
        }
        else if (Input.GetMouseButton(0))
        {
            PanCamera(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(0)) {

            //Debug.Log("===========");
            //Debug.Log("New mouse up!");
            //Debug.Log("click duration: " + click_duration);
            
            // if duration is less than max allowed click duration
            if (click_duration < max_click_duration) {
                // if we don't move left or right too much
                Vector3 current_mouse_position = Input.mousePosition;
                //vertical offset
                if (Mathf.Abs((current_mouse_position - lastPanPosition).y)/Screen.height < max_click_distance_offset) {
                    //horizontal offset
                    if (Mathf.Abs((current_mouse_position - lastPanPosition).x) / Screen.width < max_click_distance_offset)
                    {
                        Debug.Log("left click");
                        raycastClick();
                    }
                }
            }
            click_status = false;
        }

        if (click_status) {
            click_duration += Time.deltaTime;
        }
    }

    void PanCamera(Vector3 newPanPosition)
    {
        float lastY = transform.position.y;
        // Determine how much to move the camera
        Vector3 offset = cam.ScreenToViewportPoint(lastPanPosition - newPanPosition);
        Vector3 move = new Vector3(offset.x * PanSpeed, 0, offset.y * PanSpeed);

        Vector3 eangles = transform.eulerAngles;
        move = Quaternion.Euler(eangles.x, eangles.y, eangles.z) * move;

        // Perform the movement
        transform.Translate(move, Space.World);

        //change height back to saved position
        transform.position = new Vector3(transform.position.x, lastY, transform.position.z);

        // Ensure the camera remains within bounds.
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(transform.position.x, BoundsX[0], BoundsX[1]);
        pos.z = Mathf.Clamp(transform.position.z, BoundsZ[0], BoundsZ[1]);
        transform.position = pos;

        // Cache the position
        lastPanPosition = newPanPosition;
    }

    private void raycastClick() {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 100.0f) && !EventSystem.current.IsPointerOverGameObject())
        {
            // if we hit an object and pointer isn't on GUI element
            string hit_transform_name = hit.transform.name;
            Debug.Log("You selected the " + hit.transform.name); // ensure you picked right object
            if (hit_transform_name.Contains("Node(Clone)"))
            {
                // We clicked a node, get parent object and call its script for handling clicking
                hit.transform.parent.transform.GetComponent<Node>().handleUserClick();
            }
        }
    }
}