﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningController : MonoBehaviour {

    //short light variables
    private const float INTENSITY_MAX = 6.0f;
    private const float INTENSITY_SPEED = 2f;
    private const float INTENSITY_MIN = 3.0f;

    //long light variables
    private const float RANGE_MAX = 12.0f;
    private const float RANGE_SPEED = 1.0f;
    private const float RANGE_MIN = 4.0f;

    private bool top_set;
    private Light top_short;

    private bool right_set;
    private Light right_short;

    private bool bottom_set;
    private Light bottom_short;

    private bool left_set;
    private Light left_short;

	// Use this for initialization
	void Start () {
        top_set = false;
        right_set = false;
        bottom_set = false;
        left_set = false;
        Transform search = gameObject.transform.Find("T Light");
        if (search != null) {
            top_short = search.gameObject.transform.Find("T Light Source Short").GetComponent<Light>();
            top_set = true;
        }

        search = gameObject.transform.Find("R Light");
        if (search != null)
        {
            right_short = search.gameObject.transform.Find("R Light Source Short").GetComponent<Light>();
            right_set = true;
        }

        search = gameObject.transform.Find("B Light");
        if (search != null)
        {
            bottom_short = search.gameObject.transform.Find("B Light Source Short").GetComponent<Light>();
            bottom_set = true;
        }

        search = gameObject.transform.Find("L Light");
        if (search != null)
        {
            left_short = search.gameObject.transform.Find("L Light Source Short").GetComponent<Light>();
            left_set = true;
        }
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (top_set) {
            top_short.intensity = INTENSITY_MIN + Mathf.PingPong(Time.time * INTENSITY_SPEED, INTENSITY_MAX - INTENSITY_MIN);
        }
        if (right_set) {
            right_short.intensity = INTENSITY_MIN + Mathf.PingPong(Time.time * INTENSITY_SPEED, INTENSITY_MAX - INTENSITY_MIN);
        }
        if (bottom_set) {
            bottom_short.intensity = INTENSITY_MIN + Mathf.PingPong(Time.time * INTENSITY_SPEED, INTENSITY_MAX - INTENSITY_MIN);
        }
        if (left_set) {
            left_short.intensity = INTENSITY_MIN + Mathf.PingPong(Time.time * INTENSITY_SPEED, INTENSITY_MAX - INTENSITY_MIN);
        }
	}
}
